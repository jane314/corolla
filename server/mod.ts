import { getHandler, setHandler } from "./src/handlers.ts";
import { Spec } from "./src/spec.ts";
import { LiteDB } from "./src/litedb.ts";
async function serve(conn: Deno.Conn, litedb: LiteDB, spec: Spec) {
  const httpConn = Deno.serveHttp(conn);
  for await (const req of httpConn) {
    if (req.request.method === "GET") {
      getHandler(req, litedb, spec);
    } else if (req.request.method === "POST") {
      setHandler(req, litedb, spec);
    } else {
      req.respondWith(
        new Response("?", {
          status: 501,
        }),
      );
    }
  }
}

async function corolla_serve(db_path: string, spec: Spec, port: number) {
  const litedb = new LiteDB(db_path, spec);
  console.log("I will listen on", port);
  const server = Deno.listen({ port });
  for await (const conn of server) {
    serve(conn, litedb, spec);
  }
}

export { corolla_serve };
