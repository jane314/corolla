import { parse, version } from "./deps.ts";
import { corolla_serve } from "./mod.ts";
import { readSpec } from "./src/spec.ts";

async function main() {
  /*
   * CLI args
   */
  const flags = {
    "spec": {
      default: Deno.cwd() + "/spec.json",
      help: "Path to spec.json config file",
      value: "",
    },
    "db": {
      default: Deno.cwd() + "/corolla.sqlite3",
      help: "Path to SQLite DB file",
      value: "",
    },
    "port": {
      default: 50001,
      help: "Port to listen on",
      value: 0,
    },
  };
  const args = parse(Deno.args);
  if (args.help || args.h) {
    console.log(
      "corolla " + version + "\n\nHere are the CLI args and defaults:\n",
    );
    console.table(flags);
    Deno.exit(0);
  }
  flags.spec.value = args.spec ?? flags.spec.default;
  flags.db.value = args.db ?? flags.db.default;
  flags.port.value = args.port ?? flags.port.default;
  /*
   * test bed
   */
  if (args.test) {
    console.log("testing");
    Deno.exit(0);
  }
  /*
   * main execution
   */
  console.log("corolla " + version);
  const spec = await readSpec(flags.spec.value);
  if (spec === null) {
    console.log("I didn't like that spec.json");
    Deno.exit(1);
  }
  await corolla_serve(flags.db.value, spec, flags.port.value);
}

await main();
