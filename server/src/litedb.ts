import { Spec } from "./spec.ts";
import { ReqBody } from "./handlers.ts";
import { DB } from "../deps.ts";
export class LiteDB {
  private db: DB;
  private static quote(str: string) {
    return `'` + str.replaceAll(`'`, `''`) + `'`;
  }
  public constructor(db_path: string, spec: Spec) {
    this.db = new DB(db_path);
    for (const query of spec.init) {
      this.db.execute(query);
    }
    this.db.execute(
      `create table if not exists corolla 
        (key text not null unique, 
          value text, primary key('key')
        );`,
    );
    this.db.execute(
      `insert or ignore into corolla values ('version', ${
        LiteDB.quote(spec.version.toString())
      });`,
    );
    let db_version = Number(
      this.db.queryEntries(
        "select value from corolla where key = 'version';",
      )[0]?.value,
    );
    if (isNaN(db_version)) {
      console.log("Bad corolla version in database");
      Deno.exit(1);
    } else if (spec.version > db_version) {
      for (const conversion of spec.conversions) {
        if ((conversion.min <= db_version) && (db_version <= conversion.max)) {
          for (const entry of conversion.queries) {
            this.db.execute(entry);
            if (conversion.new_version !== undefined) {
              db_version = conversion.new_version;
            }
          }
        }
      }
      this.db.execute(
        `update corolla set version = ${
          LiteDB.quote(spec.version.toString())
        } where key = 'version';`,
      );
    } else if (spec.version < db_version) {
      console.log(
        `spec version ${spec.version} less than DB version ${db_version}`,
      );
      Deno.exit(1);
    } else {
      console.log("No conversion necessary");
    }
  }
  public readQuery(req: ReqBody, spec: Spec): string[][] {
    const query = spec.queries[req.query_name];
    if (query === undefined) {
      return [];
    }
    let statement = "";
    for (const entry of query.query) {
      if (typeof entry === "string") {
        statement += entry;
      } else {
        statement += LiteDB.quote(req.args[entry.arg].toString());
      }
    }
    const res: string[][] = [];
    for (const row of this.db.query<string[]>(statement)) {
      res.push(row);
    }
    return res;
  }
  public writeQuery(req: ReqBody, spec: Spec): boolean {
    const query = spec.queries[req.query_name];
    if (query === undefined) {
      return false;
    }
    let statement = "";
    for (const entry of query.query) {
      if (typeof entry === "string") {
        statement += entry;
      } else {
        statement += LiteDB.quote(req.args[entry.arg].toString());
      }
    }
    try {
      this.db.execute(statement);
      return true;
    } catch (_e) {
      return false;
    }
  }
}
