type QueryEntry = { arg: string } | string;

export interface Spec {
  version: number;
  init: string[];
  queries: {
    [query: string]: {
      friendly_name: string;
      query: QueryEntry[];
    };
  };
  conversions: {
    min: number;
    max: number;
    new_version?: number;
    queries: string[];
  }[];
}

async function readSpec(spec_path: string): Promise<Spec | null> {
  try {
    const spec_text = await Deno.readTextFile(spec_path);
    const spec: Spec = JSON.parse(spec_text);
    return spec;
  } catch {
    return null;
  }
}

export { readSpec };
