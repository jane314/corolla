import { LiteDB } from "./litedb.ts";
import { Spec } from "./spec.ts";

export interface ReqBody {
  query_name: string;
  args: {
    [key: string]: string | number | boolean;
  };
}

async function getHandler(req: Deno.RequestEvent, litedb: LiteDB, spec: Spec) {
  const body_json = (new URL(req.request.url)).searchParams.get("body");
  if (body_json !== null) {
    const body: ReqBody = JSON.parse(body_json);
    const res = litedb.readQuery(body, spec);
    return req.respondWith(Response.json(res));
  } else {
    return req.respondWith(new Response("no body?", { status: 400 }));
  }
}

async function setHandler(req: Deno.RequestEvent, litedb: LiteDB, spec: Spec) {
  const body: ReqBody = await req.request.json().catch(() => null);
  if (body !== null) {
    const res = litedb.writeQuery(body, spec);
    if (res) {
      return req.respondWith(new Response("1"));
    } else {
      return req.respondWith(new Response("bad sql", { status: 500 }));
    }
  } else {
    return req.respondWith(new Response("no body?", { status: 400 }));
  }
}

export { getHandler, setHandler };
