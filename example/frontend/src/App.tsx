import { useEffect, useState } from "react";
import "./App.css";
import { readQuery, writeQuery } from "./mod";
function App() {
  const [text, setText] = useState("");
  const [val, setVal] = useState("");
  const [hydrate, setHydrate] = useState(true);
  useEffect(() => {
    setInterval(getter, 3000);
  }, []);
  const getter = () => {
    readQuery({ "query_name": "read_t", args: {} }).then((res) =>
      setText(res.flat().join("\n"))
    );
  };

  useEffect(getter, [hydrate]);
  return (
    <div className="App">
      <pre>
      {text}
      </pre>
      <br />
      <input type="text" value={val} onChange={(e) => setVal(e.target.value)} />
      <input
        type="button"
        value="Send"
        onClick={() => {
          writeQuery({ "query_name": "insert_t", args: { a: val } }).then(
            () => {
              setHydrate(!hydrate);
            },
          );
		  setVal("");
        }}
      />
    </div>
  );
}

export default App;
