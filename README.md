# Corolla

> _Why write a stack, when you can write a JSON file._

Define an entire backend with a single `spec.json` file, consisting of SQL
functions.

# Documentation

is pretty sparse at the moment.

- See
  [spec.json](https://gitlab.com/jane314/corolla/-/blob/main/server/example/spec.json)
  for an example `spec.json`.
- See [here](https://gitlab.com/jane314/corolla/-/blob/main/client/mod.ts) for
  client API "documentation".

# Dependencies

- [Caddy](https://caddyserver.com/)
- [Deno](https://deno.land/)

# Example Application

Within `example/frontend`, run `deno task build`

Then do the following in two separate terminals:

Within `example`, run

```bash
deno run \
  --allow-read --allow-write --allow-net \
  https://gitlab.com/jane314/corolla/-/raw/main/server/server-app.ts \
  --spec spec.json --db tmp.sqlite3
```

And within `example`, run

```bash
caddy run
```

The example Caddyfile provides a good example proxy server config.
